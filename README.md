## jfile -- Examine a Text File and show Character Types

This will read a Text File and list the various type of
Characters found, for example will shows which Characters
are:
* UTF-8
* 7-bit ASCII
* ASCII Control (< 0x20)
* ASCII "upper" (> 0x7E)
* What I believe are Invalid UTF-8 values.

I have used this to examine Text Files that cause load
issues loading into various Databases that cannot handle
some Characters.  This allows one to identify the problem
characters and take action.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jfile) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jfile.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jfile.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
